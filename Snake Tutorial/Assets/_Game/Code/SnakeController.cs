﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class SnakeController : MonoBehaviour
{
	[Serializable]
	public class SnakeControllerEvent : UnityEvent<SnakeController> { }
	[Serializable]
	public class ScoreChangeEvent : UnityEvent<int> { }

	[SerializeField, Header("Settings")]
	private float _moveFrequency = 0.1f;
	[SerializeField]
	private Color _color;
	[SerializeField, Header("Managers")]
	private BoardManager _boardManager;
	[SerializeField]
	private SnakeManager _snakeManager;
	[SerializeField]
	private FoodManager _foodManager;
	[SerializeField, Header("Prefabs")]
	private SpriteRenderer _tailPrefab;
	[SerializeField, Header("Actor")]
	private Actor _actor;
	[SerializeField, Header("Events")]
	public SnakeControllerEvent _snakeDeathEvent = new SnakeControllerEvent();
	[SerializeField]
	public ScoreChangeEvent _scoreChanged = new ScoreChangeEvent();

	/// <summary>
	/// This is the score of this snake.
	/// </summary>
	private int _score = 0;
	/// <summary>
	/// This is how we know whether the snake is alive or dead.
	/// </summary>
	private bool _isAlive = true;
	/// <summary>
	/// This snake's tail.
	/// </summary>
	private List<Transform> _tail = new List<Transform>();
	private Vector3 _startPosition;
	private float _startMoveFrequency;
	private Coroutine _movement;

	public int Score
	{
		get { return _score; }
		private set
		{
			_score = value;
			_scoreChanged.Invoke(_score);
		}
	}

	public bool IsAlive
	{
		get { return _isAlive; }
	}

	public bool IsPaused { get; private set; }

	public SnakeControllerEvent OnDeath { get { return _snakeDeathEvent; } }

	public bool IsTailAt(Vector3 position)
	{
		return _tail.Any(t => t.position == position);
	}

	void Start()
	{
		GetComponent<SpriteRenderer>().color = _color;

		if (_boardManager == null || _snakeManager == null)
			gameObject.SetActive(false);

		_startPosition = transform.position;
		_startMoveFrequency = _moveFrequency;
		_snakeManager.RegisterSnake(this);
	}

	public void ResetToStart()
	{
		_isAlive = true;
		Score = 0;
		transform.position = _startPosition;
		_moveFrequency = _startMoveFrequency;
		_actor.ResetStartDirection();
	}

	public void BeginMovement()
	{
		_movement = StartCoroutine(Move());
		IsPaused = false;
	}

	public void PauseMovement()
	{
		StopCoroutine(_movement);
		IsPaused = true;
	}

	IEnumerator Move()
	{
		do
		{
			yield return new WaitForSeconds(_moveFrequency);

			if (!_isAlive)
				continue;

			var direction = _actor.GetDirection();
			var prevPos = new Vector2(transform.position.x, transform.position.y);
			var nextPos = prevPos + direction;
			if (_boardManager.IsWallAt(nextPos) || _snakeManager.IsSnakeAt(nextPos))
			{
				_isAlive = false;
				_snakeDeathEvent.Invoke(this);
				continue;
			}

			transform.Translate(direction);

			Food food;
			if (_foodManager.EatFoodAt(transform.position, out food))
			{
				var newTail = Instantiate(_tailPrefab, prevPos, Quaternion.identity);
				newTail.color = _color;
				newTail.transform.localScale = _tail.Count == 0 ? Vector2.one * 0.5f : Vector2.one * 0.75f;
				_tail.Insert(0, newTail.transform);
				Score += food.Points;
				food.PowerUp(this);
			}
			else if (_tail.Count > 0)
			{
				var tailEnd = _tail.Last();
				tailEnd.position = prevPos;
				tailEnd.localScale = Vector2.one * 0.75f;
				_tail.Insert(0, tailEnd);
				_tail.RemoveAt(_tail.Count - 1);
				_tail.Last().localScale = Vector2.one * 0.5f;
			}
		}
		while (true);
	}

	public void IncreaseSpeed()
	{
		_moveFrequency -= 0.01f;
		_moveFrequency = Mathf.Clamp(_moveFrequency, 0.05f, 0.1f);
	}

	public void ResetSpeed()
	{
		_moveFrequency = _startMoveFrequency;
	}

	public void DestroyTail()
	{
		_tail.ForEach(t => Destroy(t.gameObject));
		_tail.Clear();
	}

	public Actor Actor
	{
		get { return _actor; }
	}

	public Color Color
	{
		get { return GetComponent<SpriteRenderer>().color; }
	}
}