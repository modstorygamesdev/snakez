﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FoodManager : MonoBehaviour
{
	[SerializeField, Header("Managers")]
	private BoardManager _boardManager;
	[SerializeField, Header("Prefabs")]
	private List<Food> _foodPrefabs;
	[SerializeField, Header("Numbers")]
	private float _foodSpawnFrequency = 0.25f;

	private List<Food> _food = new List<Food>();
	private Coroutine _spawnCoroutine;

	#region Private Members

	void Start()
	{
		CreateFood();
	}

	void CreateFood()
	{
		ResetFood();

		for (int f = 0; f < _foodPrefabs.Count; f++)
		{
			for (int i = 0; i < _foodPrefabs[f].NumberOfInstances; i++)
			{
				var food = Instantiate(_foodPrefabs[f], transform, false);
				//food.OnEat.AddListener(PlayEatClip);
				_food.Add(food);
			}
		}

		_food = _food.OrderBy(f => Random.value).ToList();
	}

	IEnumerator SpawnFood()
	{
		do
		{
			yield return new WaitForSeconds(_foodSpawnFrequency);

			var food = _food.FirstOrDefault(f => !f.IsAlive);
			if (food == null)
				continue;

			Vector2 pos;
			if (!_boardManager.GetFreePosition(out pos))
				continue;

			food.transform.position = pos;
			food.IsEaten = false;
		} while (true);
	}

	#endregion
	#region Public Members

	public void BeginSpawn()
	{
		_spawnCoroutine = StartCoroutine(SpawnFood());
	}

	public void PauseSpawn()
	{
		StopCoroutine(_spawnCoroutine);
	}

	public void ResetFood()
	{
		if (_food != null && _food.Count > 0)
		{
			_food.ForEach(f => f.IsEaten = true);
			return;
		}
	}

	public bool IsFoodAt(Vector3 position)
	{
		return _food.Any(f => f.IsAlive && !f.IsEaten && f.transform.position == position);
	}

	public bool EatFoodAt(Vector3 position, out Food food)
	{
		food = _food.FirstOrDefault(f => f.transform.position == position);

		if (food == null || !food.IsAlive)
			return false;

		food.IsEaten = true;
		return true;
	}

	public int DistanceToNearestFood(Vector3 position, Vector3 direction)
	{
		int distance = _boardManager.BoardWidth;

		Food target = null;
		var pos = position;
		do
		{
			pos += direction;
			target = _food.FirstOrDefault(f => f.transform.position == pos);
			if (target)
			{
				distance = Mathf.FloorToInt(Vector2.Distance(position, target.transform.position));
				break;
			}
		}
		while (target == null && !_boardManager.IsWallAt(pos));

		return distance;
	}

	public bool IsFoodInDirection(Vector3 position, Vector3 direction)
	{
		return _food.Any(f =>
		{
			if (f.IsEaten || !f.IsAlive)
				return false;
			var pos = f.transform.position;
			return (pos.x == position.x &&
				   (direction == Vector3.up && pos.y > position.y) ||
				   (direction == Vector3.down && pos.y < position.y)) ||
				   (pos.y == position.y &&
				   (direction == Vector3.right && pos.x > position.x) ||
				   (direction == Vector3.left && pos.x < position.x));
		});
	}

	#endregion
}