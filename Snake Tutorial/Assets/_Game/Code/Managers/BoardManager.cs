﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BoardManager : MonoBehaviour
{
	[SerializeField, Header("Prefabs")]
	private Transform _wallPrefab;
	[SerializeField]
	private List<LandTile> _landPrefabs = new List<LandTile>();

	[SerializeField, Header("Managers")]
	private SnakeManager _snakeManager;
	[SerializeField]
	private FoodManager _foodManager;

	[SerializeField, Header("Numbers")]
	private int _boardWidth = 16;
	[SerializeField]
	private float _landNoiseScale = 1f;

	private List<Transform> _walls = new List<Transform>();
	private int _topBoundary;
	private int _rightBoundary;
	private int _bottomBoundary;
	private int _leftBoundary;
	private List<LandTile> _land = new List<LandTile>();

	#region Private Members

	void Start()
	{
		CreateWalls();
		CreateLandTiles();
	}

	void CreateLandTiles()
	{
		_land.ForEach(b => Destroy(b.gameObject));
		_land.Clear();

		_landPrefabs = _landPrefabs.OrderBy(l => l.NoiseThreshold).ToList();

		for (float x = 0; x <= BoardWidth; x++)
		{
			for (float y = 0; y <= BoardHeight; y++)
			{
				var sample = GetLandSampleAtPos(x, y);
				var land = _landPrefabs.LastOrDefault(l => sample >= l.NoiseThreshold);
				if (land == null)
					continue;

				var tile = Instantiate(land, transform, false);
				tile.transform.position = new Vector3(x + _leftBoundary, _topBoundary - y, 1f);
				if (tile.UseSampleColor)
					tile.GetComponent<SpriteRenderer>().color = new Color(sample, sample, sample);
				_land.Add(tile);
			}
		}
	}

	private float GetLandSampleAtPos(float x, float y)
	{
		var px = x / BoardWidth * _landNoiseScale;
		var py = y / BoardHeight * _landNoiseScale;
		return Mathf.PerlinNoise(px, py);
	}

	private void CreateWalls()
	{
		float posX = (BoardWidth / 2);
		float posY = (BoardHeight / 2);

		_rightBoundary = Mathf.FloorToInt(posX);
		_leftBoundary = -_rightBoundary;

		_topBoundary = Mathf.FloorToInt(posY);
		_bottomBoundary = -_topBoundary;

		_walls.ForEach(w => Destroy(w.gameObject));
		_walls.Clear();

		CreateWall(new Vector2(0f, posY), new Vector2(BoardWidth + 1f, 1f));
		CreateWall(new Vector2(posX, 0f), new Vector2(1f, BoardHeight));
		CreateWall(new Vector2(0f, -posY), new Vector2(BoardWidth + 1f, 1f));
		CreateWall(new Vector2(-posX, 0f), new Vector2(1f, BoardHeight));
	}

	private void CreateWall(Vector2 position, Vector2 scale)
	{
		var wall = Instantiate(_wallPrefab, transform, false);

		wall.position = position;
		wall.localScale = scale;

		_walls.Add(wall);
	}

	#endregion
	#region Public Members

	public bool GetFreePosition(out Vector2 pos)
	{
		for (int i = 0; i < 5; i++)
		{
			var posX = Random.Range(_leftBoundary + 1, _rightBoundary - 1);
			var posY = Random.Range(_bottomBoundary + 1, _topBoundary - 1);

			pos = new Vector2(posX, posY);

			if (!IsWallAt(pos) && !_foodManager.IsFoodAt(pos) && !_snakeManager.IsSnakeAt(pos))
				return true;
		}

		pos = Vector2.zero;
		return false;
	}

	public bool IsWallAt(Vector3 position)
	{
		if (position.y >= _topBoundary ||
			position.y <= _bottomBoundary ||
			position.x <= _leftBoundary ||
			position.x >= _rightBoundary)
			return true;

		return _walls.Any(w => w.position == position);
	}

	public int BoardWidth { get { return _boardWidth; } }
	public int BoardHeight { get { return _boardWidth / 2; } }

	#endregion
}