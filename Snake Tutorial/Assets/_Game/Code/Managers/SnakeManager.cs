﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class SnakeManager : MonoBehaviour
{
	[SerializeField, Header("Events")]
	private UnityEvent _snakeDeath = new UnityEvent();

	private List<SnakeController> _snakes = new List<SnakeController>();

	public void BeginSnakes()
	{
		_snakes.ForEach(s =>
		{
			s.ResetToStart();
			s.gameObject.SetActive(true);
			s.BeginMovement();
		});
	}

	public void PauseSnakes()
	{
		_snakes.ForEach(s => s.PauseMovement());
	}

	public void RegisterSnake(SnakeController snake)
	{
		if (_snakes.Contains(snake))
			return;
		snake.OnDeath.AddListener(OnSnakeDeath);
		_snakes.Add(snake);
	}

	public bool IsSnakeAt(Vector3 position)
	{
		return _snakes.Any(s => s.IsAlive && (s.transform.position == position || s.IsTailAt(position)));
	}

	public bool AreAllSnakesDead()
	{
		return !_snakes.Any(s => s.IsAlive);
	}

	public SnakeController[] Snakes { get { return _snakes.ToArray(); } }

	private void OnSnakeDeath(SnakeController snake)
	{
		_snakeDeath.Invoke();
	}
}