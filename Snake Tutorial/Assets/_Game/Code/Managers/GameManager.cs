﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

/// <summary>
/// The Game Manager is used to manage the game state.
/// </summary>
public class GameManager : MonoBehaviour
{
	[SerializeField, Header("Managers")]
	private SnakeManager _snakeManager;
	[SerializeField, Header("Leaderboard Data")]
	private ScoreDisplay[] _leaderboard;
	[SerializeField]
	private Text _winnerText;
	/// <summary>
	/// Fired when the gameplay begins.
	/// </summary>
	[SerializeField, Header("Events")]
	private UnityEvent _gamePlay = new UnityEvent();
	/// <summary>
	/// Fired when the game is paused.
	/// </summary>
	[SerializeField]
	private UnityEvent _gamePaused = new UnityEvent();
	/// <summary>
	/// Fired when the game is unpaused.
	/// </summary>
	[SerializeField]
	private UnityEvent _gameUnpaused = new UnityEvent();
	/// <summary>
	/// Fired when all of the snakez have died.
	/// </summary>
	[SerializeField]
	private UnityEvent _gameOver = new UnityEvent();

	private bool _isPaused = true;
	public bool IsPaused
	{
		get { return _isPaused; }
		set
		{
			_isPaused = value;
			if (_isPaused)
				_gamePaused.Invoke();
			else
				_gameUnpaused.Invoke();
		}
	}

	public void Play()
	{
		IsPaused = false;
		_gamePlay.Invoke();
	}

	public void GameOver()
	{
		_gameOver.Invoke();
		if (_leaderboard == null || _leaderboard.Length == 0)
			return;
		var snakes = _snakeManager.Snakes.OrderByDescending(s => s.Score).ToArray();
		for (int l = 0; l < _leaderboard.Length; l++)
		{
			_leaderboard[l].UpdateScore(snakes[l].Score);
			_leaderboard[l].Color = snakes[l].Color;
		}
		_winnerText.text = string.Format("{0} Wins!", snakes[0].name);
	}

	void Update()
	{
		if (_snakeManager.AreAllSnakesDead())
		{
			IsPaused = true;
			GameOver();
		}
	}
}