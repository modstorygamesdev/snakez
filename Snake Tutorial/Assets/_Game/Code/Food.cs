﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class Food : MonoBehaviour
{
	[SerializeField, Header("Numbers")]
	private int _points = 1;
	[SerializeField]
	private int _numberOfInstances = 1;
	[SerializeField]
	private float _durationOfLife = 0f;
	[SerializeField, Header("Events")]
	private UnityEvent _eat = new UnityEvent();
	[SerializeField]
	private UnityEvent _spawn = new UnityEvent();

	public UnityEvent OnEat { get { return _eat; } }
	public UnityEvent OnSpawn { get { return _spawn; } }

	public virtual void PowerUp(SnakeController snake) { }

	private bool _isEaten = true;
	public bool IsEaten
	{
		get { return _isEaten; }
		set
		{
			// If the food's state is not changing,
			// we don't want to call any events.
			if (_isEaten == value)
				return;
			_isEaten = value;
			CancelInvoke("Die");
			if (_isEaten)
				_eat.Invoke();
			else
			{
				IsAlive = true;
				if (_durationOfLife > 0f)
					Invoke("Die", _durationOfLife);
				_spawn.Invoke();
			}
		}
	}

	void Die()
	{
		IsAlive = false;
		// We set the field directly
		// to avoid the Eat Event from
		// triggering, because it was
		// not actually eaten.
		_isEaten = true;
	}

	public bool IsAlive
	{
		get { return gameObject.activeSelf; }
		set { gameObject.SetActive(value); }
	}

	public int Points { get { return _points; } }

	public int NumberOfInstances { get { return _numberOfInstances; } }
}