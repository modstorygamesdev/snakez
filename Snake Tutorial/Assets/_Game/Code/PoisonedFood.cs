﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PoisonedFood : Food
{
	[SerializeField, Header("Numbers")]
	private float _effectDuration = 3f;

	public override void PowerUp(SnakeController snake)
	{
		snake.StartCoroutine(StopSnake(snake));
	}

	IEnumerator StopSnake(SnakeController snake)
	{
		snake.PauseMovement();

		yield return new WaitForSeconds(_effectDuration);

		var manager = FindObjectOfType<GameManager>();
		if (!manager.IsPaused)
			snake.BeginMovement();
	}
}