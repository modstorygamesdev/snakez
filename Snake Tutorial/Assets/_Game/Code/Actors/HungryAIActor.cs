﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HungryAIActor : AIActor
{
	private int _shortestDistance = int.MaxValue;

	protected override void BeginDirectionCalculations()
	{
		_shortestDistance = int.MaxValue;
	}

	protected override bool IsDirectionValid(Vector2 position, Vector2 direction)
	{
		var distanceToFood = _foodManager.DistanceToNearestFood(position, direction);
		if (distanceToFood < _shortestDistance)
		{
			_shortestDistance = distanceToFood;
			return true;
		}
		return false;
	}

	void OnDrawGizmos()
	{
		if (_foodManager == null)
			return;
		var pos = new Vector2(transform.position.x, transform.position.y);
		var distance = _foodManager.DistanceToNearestFood(pos, _direction);
		Gizmos.color = Color.yellow;
		Gizmos.DrawSphere(pos + _direction * distance, 0.35f);
	}
}