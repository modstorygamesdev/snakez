﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class AIActor : Actor
{
	[SerializeField, Header("Managers")]
	protected BoardManager _boardManager;
	[SerializeField]
	protected SnakeManager _snakeManager;
	[SerializeField]
	protected FoodManager _foodManager;
	[SerializeField, Range(0.1f, 1f)]
	protected float _chanceToChangeDirection = 0.75f;
	[SerializeField]
	protected Vector2 _direction = Vector2.left;
	[SerializeField, Range(0f, 1f)]
	protected float _chanceToRandomizeDirectionArray = 0.5f;

	private Vector2 _startDirection;

	private Vector2[] _directions = new Vector2[]
	{
		Vector2.right, Vector2.up, Vector2.left, Vector2.down,
	};

	void Start()
	{
		_startDirection = _direction;
	}

	public override Vector2 GetDirection()
	{
		if (Random.value <= 1f - _chanceToChangeDirection)
			return _direction;

		BeginDirectionCalculations();

		var prevPos = new Vector2(transform.position.x, transform.position.y);
		if (Random.value <= 1f - _chanceToRandomizeDirectionArray)
			RandomizeDirectionArray();
		for (int i = 0; i < _directions.Length; i++)
		{
			var newDir = _directions[i];
			if (_direction == -newDir)
				continue;
			var nextPos = prevPos + newDir;
			if (!IsAreaClear(nextPos))
				continue;
			if (IsDirectionValid(prevPos, newDir))
				_direction = newDir;
		}

		return _direction;
	}

	protected void RandomizeDirectionArray()
	{
		_directions = _directions.OrderBy(v => Random.value).ToArray();
	}

	protected virtual void BeginDirectionCalculations() { }

	protected abstract bool IsDirectionValid(Vector2 position, Vector2 direction);

	protected bool IsAreaClear(Vector3 pos)
	{
		return !_boardManager.IsWallAt(pos) && !_snakeManager.IsSnakeAt(pos);
	}

	public override void ResetStartDirection()
	{
		_direction = _startDirection;
	}
}