﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RandomActor : Actor
{
	[SerializeField]
	private float _chanceOfChange = 1f;

	private Vector2[] _directions = new Vector2[]
	{
		Vector2.up, Vector2.down, Vector2.right, Vector2.left
	};

	private int _currentDirection = 0;

	public override Vector2 GetDirection()
	{
		if (Random.Range(0f, 1f) + _chanceOfChange < 0.75f)
			return _directions[_currentDirection];

		_currentDirection = Random.Range(0, _directions.Length - 1);
		return _directions[_currentDirection];
	}
}