﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class UnityInputActor : Actor
{
	[SerializeField, Header("Key Codes")]
	private KeyCode _upKey = KeyCode.W;
	[SerializeField]
	private KeyCode _rightKey = KeyCode.D;
	[SerializeField]
	private KeyCode _downKey = KeyCode.S;
	[SerializeField]
	private KeyCode _leftKey = KeyCode.A;
	[SerializeField]
	private Vector2 _direction = Vector2.right;

	private Queue<Vector2> _directionChanges = new Queue<Vector2>();
	private Vector2 _startDirection;
	private SnakeController _snake;

	public override Vector2 GetDirection()
	{
		if (_directionChanges.Count > 0)
			_direction = _directionChanges.Dequeue();

		return _direction;
	}

	void Start()
	{
		_startDirection = _direction;

		_snake = GetComponent<SnakeController>();
	}

	void Update()
	{
		if (_snake != null && _snake.IsPaused)
		{
			_directionChanges.Clear();
			return;
		}

		var prevDir = _directionChanges.Count == 0 ? _direction : _directionChanges.Last();
		var newDir = prevDir;
		if (prevDir != Vector2.down && Input.GetKeyDown(_upKey))
			newDir = Vector2.up;
		else if (prevDir != Vector2.up && Input.GetKeyDown(_downKey))
			newDir = Vector2.down;
		else if (prevDir != Vector2.right && Input.GetKeyDown(_leftKey))
			newDir = Vector2.left;
		else if (prevDir != Vector2.left && Input.GetKeyDown(_rightKey))
			newDir = Vector2.right;

		if (newDir != prevDir)
			_directionChanges.Enqueue(newDir);
	}

	public override void ResetStartDirection()
	{
		_direction = _startDirection;
	}
}