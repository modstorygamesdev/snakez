﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public abstract class Actor : MonoBehaviour
{
	public abstract Vector2 GetDirection();

	public virtual void ResetStartDirection() { }
}