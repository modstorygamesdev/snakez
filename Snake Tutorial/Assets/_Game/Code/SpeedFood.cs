﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SpeedFood : Food
{
	[SerializeField, Header("Numbers")]
	private float _effectDuration = 3f;

	public override void PowerUp(SnakeController snake)
	{
		snake.StartCoroutine(IncreaseSnakeSpeed(snake));
	}

	IEnumerator IncreaseSnakeSpeed(SnakeController snake)
	{
		snake.IncreaseSpeed();

		yield return new WaitForSeconds(_effectDuration);

		var manager = FindObjectOfType<GameManager>();
		if (!manager.IsPaused)
			snake.ResetSpeed();
	}
}