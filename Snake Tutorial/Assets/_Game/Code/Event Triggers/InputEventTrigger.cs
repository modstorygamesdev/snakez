﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class InputEventTrigger : MonoBehaviour
{
	[System.Serializable]
	public class InputEventSettings
	{
		[SerializeField]
		private KeyCode _keyCode = KeyCode.Space;
		[SerializeField]
		private UnityEvent _keyDown = new UnityEvent();
		[SerializeField]
		private UnityEvent _keyUp = new UnityEvent();

		public void Process()
		{
			if (Input.GetKeyDown(_keyCode))
				_keyDown.Invoke();
			else if (Input.GetKeyUp(_keyCode))
				_keyUp.Invoke();
		}
	}

	[SerializeField]
	private List<InputEventSettings> _inputEventSettings;

	void Update()
	{
		_inputEventSettings.ForEach(s => s.Process());
	}
}