﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class LandTile : MonoBehaviour
{
	[SerializeField]
	private float _minPulseRate = 0.1f;
	[SerializeField]
	private float _maxPulseRate = 0.9f;
	[SerializeField]
	private Color _pulseColor = Color.white;
	[SerializeField]
	private float _noiseThreshold = 0.5f;
	[SerializeField]
	private bool _useSampleColor = true;

	private SpriteRenderer _renderer;
	private Color _startColor;

	void Start()
	{
		_renderer = GetComponent<SpriteRenderer>();
		_startColor = _renderer.color;

		StartCoroutine(Pulse());
	}

	IEnumerator Pulse()
	{
		while (true)
		{
			var pulseTime = Random.Range(_minPulseRate, _maxPulseRate);
			var pulseIn = Pulse(_startColor, _pulseColor, pulseTime);
			while (pulseIn.MoveNext())
				yield return pulseIn.Current;
			var pulseOut = Pulse(_pulseColor, _startColor, pulseTime);
			while (pulseOut.MoveNext())
				yield return pulseOut.Current;
		}
	}

	IEnumerator Pulse(Color from, Color to, float pulseTime)
	{
		var time = Time.realtimeSinceStartup;
		do
		{
			var t = Time.realtimeSinceStartup - time;
			if (t >= pulseTime)
				break;
			_renderer.color = Color.Lerp(from, to, t / pulseTime);
			yield return null;
		}
		while (true);
	}

	public bool UseSampleColor { get { return _useSampleColor; } }

	public float NoiseThreshold { get { return _noiseThreshold; } }
}