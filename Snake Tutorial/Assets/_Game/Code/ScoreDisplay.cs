﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ScoreDisplay : MonoBehaviour
{
	[SerializeField]
	private Text _scoreText;

	public void UpdateScore(int value)
	{
		_scoreText.text = value.ToString("0000000");
	}

	public Color Color
	{
		get { return _scoreText.color; }
		set { _scoreText.color = value; }
	}
}